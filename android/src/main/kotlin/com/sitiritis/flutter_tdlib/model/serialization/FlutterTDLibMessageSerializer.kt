package com.sitiritis.flutter_tdlib.model.serialization

import com.google.gson.*
import com.google.gson.reflect.TypeToken
import com.sitiritis.flutter_tdlib.model.FlutterTDLibMessage
import org.drinkless.td.libcore.telegram.TdApi
import java.lang.reflect.Type

class FlutterTDLibMessageSerializer(
  private val constructorClassMap: Map<Int, Class<*>>
): JsonSerializer<FlutterTDLibMessage>, JsonDeserializer<FlutterTDLibMessage>
{
  companion object
  {
    val flutterTDLibMessageType = FlutterTDLibMessage::class.java
    val listFlutterTDLibMessageType =
      object : TypeToken<List<FlutterTDLibMessage>>() {}.type
  }

  override fun serialize(
      src: FlutterTDLibMessage?,
      typeOfSrc: Type?,
      context: JsonSerializationContext?
  ): JsonElement
  {
    val result = JsonObject()

    src?.let {
      result.apply {
        addProperty(
            "message",
            GsonBuilder()
                .excludeFieldsWithModifiers(java.lang.reflect.Modifier.TRANSIENT)
                .create()
                .toJson(
                    src.message,
                    constructorClassMap[src.message.constructor]
                )
        )
        addProperty("messageID", it.messageID)
      }
    }

    return result
  }

  override fun deserialize(
      json: JsonElement?,
      typeOfT: Type?,
      context: JsonDeserializationContext?
  ): FlutterTDLibMessage
  {
    val flutterTDLibMessageJSONObject = json?.asJsonObject

    val messageID: Long = flutterTDLibMessageJSONObject?.get("messageID")?.asLong!!
    val messageJSONElement = flutterTDLibMessageJSONObject.get("message")
    val typeID: Int =
      messageJSONElement
        .asJsonObject
        .get("CONSTRUCTOR")
        .asInt

    return FlutterTDLibMessage(
        (
            GsonBuilder()
                .create()
                .fromJson(messageJSONElement, constructorClassMap[typeID])
                as TdApi.Object
            ),
        messageID
    )
  }
}
