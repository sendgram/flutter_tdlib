package com.sitiritis.flutter_tdlib.model

import org.drinkless.td.libcore.telegram.TdApi

data class FlutterTDLibMessage(
  val message: TdApi.Object,
  val messageID: Long = 0
)
