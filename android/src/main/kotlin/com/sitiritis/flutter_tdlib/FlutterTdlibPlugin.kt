package com.sitiritis.flutter_tdlib

import com.google.gson.Gson
import com.google.gson.GsonBuilder

import kotlin.concurrent.thread
import java.util.concurrent.locks.ReentrantLock
import android.util.Log

import com.sitiritis.flutter_tdlib.model.FlutterTDLibMessage
import com.sitiritis.flutter_tdlib.model.serialization.FlutterTDLibMessageSerializer

import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar

import org.drinkless.td.libcore.telegram.TdApi
import org.drinkless.td.libcore.telegram.NativeClient

class FlutterTdlibPlugin(private val channel: MethodChannel): MethodCallHandler
{
  private val channelLock = ReentrantLock()
  private val receiveLock = ReentrantLock()

  private val clientsReceiveThreads: MutableMap<Long, Thread> = HashMap()

  private val constructorClassMap: Map<Int, Class<*>>
  private val gson: Gson

  private val MAX_EXENTS = 1000
  private val events = Array<TdApi.Object?>(MAX_EXENTS) { null }
  private val eventIDs = LongArray(MAX_EXENTS) { 0 }

  init
  {
    val constructorClassMapTemp: MutableMap<Int, Class<*>> = HashMap()

    val models = TdApi::class.java.declaredClasses.filter {
      it.declaredFields.filter {
        it.name == "CONSTRUCTOR"
      }.isNotEmpty()
    }

    for (model: Class<*> in models)
    {
      constructorClassMapTemp[model.getDeclaredField("CONSTRUCTOR").getInt(null)] =
        model
    }

    constructorClassMap = constructorClassMapTemp
    gson =
      GsonBuilder()
        .registerTypeAdapter(
          FlutterTDLibMessageSerializer.flutterTDLibMessageType,
          FlutterTDLibMessageSerializer(constructorClassMap)
        )
        .create()
  }

  companion object
  {
    @JvmStatic
    fun registerWith(registrar: Registrar)
    {
      val channel = MethodChannel(registrar.messenger(), "flutter_tdlib")
      channel.setMethodCallHandler(FlutterTdlibPlugin(channel))
    }
  }

  override fun onMethodCall(call: MethodCall, result: Result)
  {
    when (val methodName = call.method)
    {
      "createClient" ->
      {
        val clientID: Long = NativeClient.createClient()
        clientsReceiveThreads[clientID] = thread(
          start = false,
          isDaemon = false,
          name = "TDClient${clientID}ReceiveThread",
          block = createReceiveClosureForClientID(clientID)
        )
        result.success(clientID)
      }
      "startReceive" ->
      {
        val clientID = (call.arguments as Long)
        clientsReceiveThreads[clientID]?.start()

        result.success(true)
      }
      "destroyClient" ->
      {
        val clientID = (call.argument<Long>("clientID"))!!
        val queryID = (call.argument<Long>("queryID"))!!

        NativeClient.clientSend(clientID, queryID, TdApi.Close())

        result.success(true)
      }
      "send", "execute" ->
      {
        val clientID: Long? = call.argument<Long>("clientID")
        val request: String = call.argument<String>("request")!!
        val flutterTDLibMessage =
          gson
            .fromJson(
              request,
              FlutterTDLibMessageSerializer.flutterTDLibMessageType
            )

        when (methodName)
        {
          "send" ->
          {
            NativeClient.clientSend(
              clientID!!,
              flutterTDLibMessage.messageID,
              flutterTDLibMessage.message
            )

            result.success(true)
          }
          "execute" ->
          {
            result.success(
              gson.toJson(FlutterTDLibMessage(
                NativeClient.clientExecute(flutterTDLibMessage.message)
              ))
            )
          }
        }
      }
      else -> result.notImplemented()
    }
  }

  private fun createReceiveClosureForClientID(clientID: Long): () -> Unit =
  {
    try
    {
//      Log.d(
//        "flutter_tdlib",
//        "Receiving from thread: ${Thread.currentThread().name}. Cleint ID = ${clientID}"
//      )

      while (!(Thread.currentThread().isInterrupted))
      {
//        Log.d(
//          "flutter_tdlib",
//          "Call receive: ${Thread.currentThread().name}. Cleint ID = ${clientID}"
//        )

        receiveQueries(clientID, 300.0)?.let {
          sendResponseJSONToFlutter(clientID, it)
        }
      }
    }
    catch (e: Exception)
    {
      Log.d(
        "flutter_tdlib",
        "Exception occured in: ${Thread.currentThread().name}. Cleint ID = ${clientID}. Exception: ${e.localizedMessage}"
      )
    }
    finally
    {
//      Log.d(
//        "flutter_tdlib",
//        "Closing client $clientID. Thread interrupted flag: ${Thread.currentThread().isInterrupted}"
//      )

      NativeClient.destroyClient(clientID)
      channel.invokeMethod("onClientClosed", clientID)
    }
  }

  private fun receiveQueries(clientID: Long, timeout: Double): String?
  {
    try
    {
      receiveLock.lock()

      val numEvents =
        NativeClient.clientReceive(
          clientID,
          eventIDs,
          events,
          timeout
        )

      var resultJSON: String? = null

      if (numEvents > 0)
      {
        val eventsMessage =
          events
            .take(numEvents)
            .zip(eventIDs.toTypedArray()) { event, eventID ->
              if (
                eventID == 0L &&
                event is TdApi.UpdateAuthorizationState &&
                event.authorizationState is TdApi.AuthorizationStateClosed
              )
              {
                Thread.currentThread().interrupt()
              }

              FlutterTDLibMessage(event!!, eventID)
            }

        resultJSON =
          gson.toJson(
            eventsMessage,
            FlutterTDLibMessageSerializer.listFlutterTDLibMessageType
          )

        for (i in 0..numEvents) { events[i] = null }
      }

      return resultJSON
    }
    catch (e: Exception)
    {
      throw e
    }
    finally
    {
      receiveLock.unlock()
    }
  }

  private fun sendResponseJSONToFlutter(clientID: Long, jsonToSend: String)
  {
    try
    {
      channelLock.lock()

      channel.invokeMethod(
        "processReceived",
          arrayListOf<String>(clientID.toString(), jsonToSend)
      )
    }
    catch (e: Exception)
    {
      throw e
    }
    finally
    {
      channelLock.unlock()
    }
  }
}
