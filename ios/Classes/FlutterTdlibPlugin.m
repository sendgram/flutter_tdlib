#import "FlutterTdlibPlugin.h"
#import <flutter_tdlib/flutter_tdlib-Swift.h>

@implementation FlutterTdlibPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlutterTdlibPlugin registerWithRegistrar:registrar];
}
@end
