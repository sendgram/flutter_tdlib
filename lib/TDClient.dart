import 'dart:async';

import 'TDAPI.dart';
import 'src/TDClientEventHandler.dart';
import 'src/flutter_tdlib.dart';
import 'src/TDLibMessage.dart';

class TDClient
{
  static Future<TDClient> createInstance([
    void updateHandler(TDObject resultToHandle),
    void exceptionHandler(Exception ex)
  ]) async
  {
    final TDClient result = TDClient._(
      await FlutterTdlib.createClient(),
      updateHandler,
      exceptionHandler
    );

    await FlutterTdlib.setResponseHandlerForClient(
      result._clientID,
      TDClientEventHandler(
        result._processResponse,
        result._onClosed
      )
      
    );

    return result;
  }

  TDClient._(
    this._clientID,
    this.updateHandler,
    this.exceptionHandler
  );

  Future<void> close() async
  {
    return Future(() async {
      assert(isOpened,
      "TDClient is already closed. An instance of TDClient must be closed exactly once.");

      await FlutterTdlib.destroyClient(_clientID, _incrementAndGetQueryID());
      // TODO: complete all remaning pending queries with error that client is closed
      // for (int queryID in _pendingQueries.keys)
      // {
      //   _pendingQueries.remove(queryID).complete(/*TDError(500, "Client is closed")*/);
      // }

      return closeCompleter.future;
    });
  }


  Future<TDObject> send(TDFunction query, [
    void resultHandler(TDObject resultToHandle),
    void exceptionHandler(Exception ex)
  ]) async
  {
    return Future(() async {
      assert(isOpened, "TDClient instance is already closed.");

      final int curQueryID = _incrementAndGetQueryID();
      final curQueryCompleter = Completer<TDObject>();
      _pendingQueries[curQueryID] = curQueryCompleter;

      await FlutterTdlib.send(_clientID, curQueryID, query);

      return curQueryCompleter.future;
    });
  }

  Future<TDObject> execute(TDFunction query) async
  {
    return Future(() {
      assert(isOpened, "TDClient instance is already closed.");

      return FlutterTdlib.execute(query);
    });
  }


  bool get isClosed => closeCompleter.isCompleted;
  bool get isOpened => !isClosed;


  void _processResponse(TDLibResponse response)
  {
    if (response.messageID == 0)
    {
      try
      {
        updateHandler(response.message);
      }
      catch (e)
      {
        if (exceptionHandler != null)
        {
          try
          {
            exceptionHandler(e);
          }
          catch (ignore) {}
        }
      }
    }
    else
    {
      _pendingQueries.remove(response.messageID)?.complete(response.message);
    }
  }

  void _onClosed()
  {
    closeCompleter.complete();
  }

  int _incrementAndGetQueryID()
  {
    return ((++_queryID) == 0) ? ++_queryID : _queryID;
  }


  final int _clientID;
  final Completer<void> closeCompleter = Completer<void>();

  int _queryID = 0;

  void Function(TDObject resultToHandle) updateHandler;
  void Function(Exception ex) exceptionHandler;

  final _pendingQueries = Map<int, Completer<TDObject>>();
}
