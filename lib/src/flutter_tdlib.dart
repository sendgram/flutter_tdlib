import 'dart:async';
import 'dart:convert';

import 'package:flutter/services.dart';

import 'package:flutter_tdlib/TDAPI.dart';
import 'TDLibMessage.dart';
import 'TDClientEventHandler.dart';

class FlutterTdlib
{
  static final Map<int, TDClientEventHandler> _clientsResponseHandlers = Map();

  static final MethodChannel _channel = _getPluginChannel();

  static MethodChannel _getPluginChannel()
  {
    final MethodChannel resultChannel = MethodChannel('flutter_tdlib');
    resultChannel.setMethodCallHandler((MethodCall call) {
      switch (call.method)
      {
        case "processReceived":
          final int clientID = int.parse(call.arguments[0]);
          final String receivedResultsJSON = call.arguments[1];
          List<dynamic> decodedResponses = jsonDecode(receivedResultsJSON);
          for (Map<String, dynamic> decodedResponse in decodedResponses)
          {
            TDLibResponse response = TDLibResponse.fromDecodedJSON(decodedResponse);
            _clientsResponseHandlers[clientID].receiveResponseHandler(response);
          }

          break;
        case "onClientClosed":
          final int clientID = int.parse(call.arguments);
          _clientsResponseHandlers.remove(clientID);
          _clientsResponseHandlers[clientID].closeHandler();

          break;
        default:
          throw MissingPluginException(
            "[flutter_tdlib]: Platform tried to call an unknown method through a platform channel: ${call.method}."
          );
      }
    });

    return resultChannel;
  }

  static Future<int> createClient() async =>
    await _channel.invokeMethod("createClient");

  static Future<void> setResponseHandlerForClient(int clientID, TDClientEventHandler eventHandler) async
  {
    assert(clientID != null, "clientID passed to FlutterTdlib.setResponseHandlerForClient must not be null");
    assert(eventHandler != null, "eventHandler passed to FlutterTdlib.setResponseHandlerForClient must not be null");
    
    _clientsResponseHandlers[clientID] = eventHandler;
    await _channel.invokeMethod("startReceive", clientID);
  }

  static Future<void> destroyClient(int clientID, int queryID) async
  {
    assert(clientID != null, "clientID passed to FlutterTdlib.close must not be null");

    await _channel.invokeMethod("destroyClient",
      {
        "clientID": clientID,
        "queryID": queryID
      }
    );
  }

  static Future<void> send(int clientID, int queryID, TDFunction query) async
  {
    assert(clientID != null, "clientID passed to FlutterTdlib.send must not be null");
    assert(query != null, "query passed to FlutterTdlib.send must not be null");

    await _channel.invokeMethod(
      "send",
      {
        "clientID": clientID,
        "request": TDLibRequest(query, queryID).toJSON()
      }
    );
  }

  static Future<TDObject> execute(TDFunction query) async
  {
    assert(query != null, "query passed to FlutterTdlib.execute must not be null");

    return TDObject.fromJSON(
      await _channel.invokeMethod("execute", TDLibRequest(query).toJSON())
    );
  }
}
