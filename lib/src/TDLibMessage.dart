import '../TDAPI.dart' as td;

abstract class TDLibMessage
{
  TDLibMessage(this.message, this.messageID);

  final td.TDObject message;
  final int messageID;
}

class TDLibRequest extends TDLibMessage
{
  // queryID == 0 - execute, otherwise - send
  TDLibRequest(td.TDObject query, [int queryID = 0]) : super(query, queryID);
  String toJSON() => "{\"messageID\":$messageID,\"message\":${message.toJSON()}}";
}

class TDLibResponse extends TDLibMessage
{
  // queryID == 0 - update, otherwise - response to previously sent request
  TDLibResponse(td.TDObject query, int queryID) : super(query, queryID);
  TDLibResponse.fromDecodedJSON(Map<String, dynamic> decodedJSON) :
    super(td.TDObject.fromJSON(decodedJSON["message"]), decodedJSON["messageID"]);
}
