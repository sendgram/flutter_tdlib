import 'TDLibMessage.dart';

class TDClientEventHandler
{
  TDClientEventHandler(
    this.receiveResponseHandler,
    this.closeHandler
  );

  void Function(TDLibResponse response) receiveResponseHandler;
  void Function() closeHandler;
}
