import 'package:flutter/material.dart';

import 'package:flutter_tdlib/TDClient.dart';

TDClient client;

void main() async
{
  runApp(MyApp());

  client = await TDClient.createInstance();
  // TODO send request with initial data
  client.close();
}

// TODO:
// https://flutter.dev/docs/get-started/flutter-for/android-devs#how-do-i-listen-to-android-activity-lifecycle-events
class MyApp extends StatelessWidget
{
  @override
  Widget build(BuildContext context)
  {
    return MaterialApp(
      home: Scaffold(
        body: PhoneNumberRoute(),
      ),
    );
  }
}


class PhoneNumberRoute extends StatelessWidget
{
  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
      appBar: AppBar(
        title: Text("flutter_tdlib example - Log in - Phone number"),
      ),
      body: ListView(
        children: <Widget>[
          Center(child: Text("Enter your phone number")),
          Center(
            child: TextField(
              autofocus: true,
              textAlign: TextAlign.center,
            )
          ),
          Center(
            child: RaisedButton(
              child: Text("Log in"),
              onPressed: () {
                // TODO: send number, if it is correct - go to the next screen
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => EnterCodeRoute()
                  )
                );
              }
            ),
          )
        ],
      ),
    );
  }
}

class EnterCodeRoute extends StatelessWidget
{
  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
      appBar: AppBar(
        title: Text("flutter_tdlib example - Log in - Verification code"),
      ),
      body: ListView(
        children: <Widget>[
          Center(child: Text("Enter the verification code")),
          Center(
            child: TextField(
              autofocus: true,
              textAlign: TextAlign.center,
            )
          ),
          Center(
            child: RaisedButton(
              child: Text("Submit"),
              onPressed: () {

              }
            ),
          )
        ],
      ),
    );
  }
}
